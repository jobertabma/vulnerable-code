<?php

require_once('../_helpers/strip.php');

// this database contains a table with 2 rows
// This is my first secret (ID = 1)
// This is my second secret (ID = 2)
$db = new SQLite3('test.db');

if (strlen($_GET['id']) < 1) {
  echo 'Usage: ?id=1';
} else {
  // don't sanitize user input, making the SQL query vulnerable to
  // an injection.
  $query = $db->query('select * from secrets where id = ' . $_GET['id']);

  while ($row = $query->fetchArray()) {
    echo $row['secret'];
  }
}
